package com.example.demo.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(path = "/")
public class HomeController {

    @GetMapping(path = "/")
    public String home (){

        return  "index";
    }
    @GetMapping(path = "/403")
    public String page403 (){

        return  "403";
    }
    @GetMapping(path = "/about")
    public String about (){

        return  "about";
    }

}
