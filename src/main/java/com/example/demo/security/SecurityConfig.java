package com.example.demo.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
public class SecurityConfig  extends WebSecurityConfigurerAdapter {

   @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
     PasswordEncoder passwordEnc=  passwordEncoder();
        auth.inMemoryAuthentication().withUser("user1").password(passwordEnc.encode("adminadmin")).roles("USER");
       auth.inMemoryAuthentication().withUser("user2").password(passwordEnc.encode("adminadmin")).roles("ADMIN");
       auth.inMemoryAuthentication().withUser("user3").password(passwordEnc.encode("adminadmin")).roles("ADMIN","USER");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.formLogin();
        http.authorizeRequests().antMatchers("/patients/save**/**","/patients/delete**/**").hasRole("ADMIN");
       // http.authorizeRequests().antMatchers("/patients/delete**/**").hasAnyRole("USER","ADMIN");
        http.authorizeRequests().anyRequest().authenticated();
        http.exceptionHandling().accessDeniedPage("/403");


    }
    @Bean
    public PasswordEncoder passwordEncoder(){ return new BCryptPasswordEncoder();}
}
