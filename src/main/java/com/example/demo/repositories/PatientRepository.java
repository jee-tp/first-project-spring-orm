package com.example.demo.repositories;

import com.example.demo.entities.Patient;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;


public interface PatientRepository extends JpaRepository<Patient,Long> {



 public Page<Patient> findByNomContains(String name, Pageable pageable);
 public    List<Patient> findByNomContainsAndMalade(String name,boolean malade);
 @Query("SELECT p.nom,p.dateDeNaissance,SUM(p.score) from  Patient p where p.id = ?1")
 public Patient findSumScorePatient(int name);



 }


