package com.example.demo;

import com.example.demo.entities.Patient;
import com.example.demo.repositories.PatientRepository;
import org.apache.coyote.Request;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@SpringBootApplication
public class FirstProjectSpringApplication implements CommandLineRunner {

    @Autowired
    private PatientRepository patientRepository ;

    public static void main(String[] args) {
        SpringApplication.run(FirstProjectSpringApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {

        List<Patient>patientList=new ArrayList<>();
        for(int i=1;i<=100;i++)
      patientList.add(new Patient(null,"Yassine"+i,new Date(),2300,false));

        patientRepository.saveAll(patientList);
        System.out.println("*********************************************");
        patientRepository.findAll().forEach(patient ->
        {System.out.println(patient.toString());});
        System.out.println("*********************************************");
        Patient patient= patientRepository.findById(100L).get();
        System.out.println(patient.toString());
        System.out.println("*********************************************");

        //find Patients By name
      /*  patientRepository.findByNomContains("0").forEach(p ->
        {System.out.println(p.toString());});*/

        //Delete method
      /* patientRepository.deleteById(99L);

       //Find all patient
        Page<Patient> patientPage = patientRepository.findAll(PageRequest.of(0,3));
        List<Patient> patientPageContent = patientPage.getContent();
       
       patientPageContent.forEach(p2 ->
        {System.out.println(p2.toString());});

       Patient p5=patientRepository.findSumScorePatient(90);*/





    }
}
