package com.example.demo.controller;

import com.example.demo.entities.Patient;
import com.example.demo.repositories.PatientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;

//@RestController
@Controller
@RequestMapping(path = "/patients")
public class PatientsController {

    @Autowired
    private PatientRepository patientRepository;


    @GetMapping(path = "/list")
    public String patientList (Model model,
                               @RequestParam(name = "page",defaultValue = "0")int page,
                               @RequestParam(name = "size",defaultValue = "5") int size,
                               @RequestParam(name = "keyword",defaultValue = "") String keyword){

        Page<Patient> patientPage=patientRepository.findByNomContains(keyword,PageRequest.of(page,size));
        model.addAttribute("patients",patientPage.getContent());
        model.addAttribute("pages",new int[patientPage.getTotalPages()]);
        model.addAttribute("currentPage",page);
        model.addAttribute("keyword",keyword);
        model.addAttribute("size",size);
        return "patients";
      //  return  patientRepository.findAll();
    }
    @GetMapping(path = "/delete")
    public String delete (Model model,Long id, int page,int size,String keyword){

        patientRepository.deleteById(id);

        return "redirect:/patients/list?page="+page+"&keyword="+keyword+"&size="+size;
        //return  patientList(model,page,size,keyword);
    }

    @GetMapping(value="/form")
    public String form(Model model){
        model.addAttribute("patient", new Patient());
        model.addAttribute("mode","new");
        return "formPatient";
    }

    @PostMapping(value="/save")
    public String save(Model model, @Valid Patient p, BindingResult bindingResult){
        if(bindingResult.hasErrors()){ return "formPatient"; }
        patientRepository.save(p);
        model.addAttribute("patient",p);
        return "confirmation";
    }

    @GetMapping(value="/edit")
    public String edit(Model model,Long id){
        Patient p=patientRepository.findById(id).get();
        model.addAttribute("patient", p);
        model.addAttribute("mode","edit");
        return "formPatient";
    }

}
